// require('./bootstrap');
import vue from 'vue'
import VueMeta from 'vue-meta'
import './assets/css/style.css'

// Start Frontend CSS 
import './assets/frontend/css/slick.css'
import './assets/frontend/css/animate.css'
import './assets/frontend/css/nice-select.css'
import './assets/frontend/css/jquery.nice-number.min.css'
import './assets/frontend/css/magnific-popup.css'
import './assets/frontend/css/bootstrap.min.css'
import './assets/frontend/css/font-awesome.min.css'
import './assets/frontend/css/default.css'
import './assets/frontend/css/responsive.css'






window.Vue = vue;

import App from './components/App.vue';
import VueRouter from 'vue-router';
import VueAxios from 'vue-axios';
import axios from 'axios';
import {routes} from './routes';
 
Vue.use(VueRouter);
Vue.use(VueMeta);
Vue.use(VueAxios, axios);
 
const router = new VueRouter({
    mode: 'history',
    routes: routes
});
 
const app = new Vue({
    el: '#app',
    router: router,
    render: h => h(App),
});

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);
