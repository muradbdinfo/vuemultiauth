const App = () => import('./components/App.vue' /* webpackChunkName: "resource/js/components/header" */)
// const Footer = () => import('./components/Footer.vue' /* webpackChunkName: "resource/js/components/header" */)
// const Header = () => import('./components/Header.vue' /* webpackChunkName: "resource/js/components/header" */)
const Contact = () => import('./components/Contact.vue' /* webpackChunkName: "resource/js/components/header" */)

export const routes = [
    {
        
        path: '/',
        component: App
    },
    {
        name: 'contact',
        path: '/contact',
        component: Contact
    },
   
]